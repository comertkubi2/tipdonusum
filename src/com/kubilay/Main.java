package com.kubilay;

public class Main {

    static Ogretmen ogretmen1; //global field
    static Ogretmen ogretmen2;
    static Ogretmen ogretmen3;
    static Ogretmen ogretmen4;
    static Ogretmen ogretmen5;

    public static void main(String[] args) {
	// write your code here
       /* Donusum1 donusum1=new Donusum1();
        donusum1.topla1();
        donusum1.topla2();
        donusum1.topla3();
        donusum1.topla4(); */

      //  Diziler diziler=new Diziler();
      //  diziler.dizi1();
      //  diziler.dizi2();
        Main.ogretmenOlustur();
        Main.ogretmenBilgileriniGoster();
        Main.ogretmenBilgiObjecttenGetir(); //önüne Main yazmasakta olur çünkü Main in içindeyiz
    }

    public static void ogretmenOlustur(){
        ogretmen1=new Ogretmen("levent","albayrak","bilişim");
        ogretmen2=new Ogretmen();
        ogretmen3=new Ogretmen("murat","sağlık");
        ogretmen4=new Ogretmen("cumali","akgün","bilişim");
        ogretmen5=new Ogretmen("dilek","özövgü","bilişim");

    }

    public static void ogretmenBilgileriniGoster(){
        ogretmen1.ogretmenDenemeMetot()  ;
        ogretmen2.ogretmenDenemeMetot();
        ogretmen3.ogretmenDenemeMetot();
        ogretmen4.ogretmenDenemeMetot();
        ogretmen5.ogretmenDenemeMetot();
    }

    public static void ogretmenBilgiObjecttenGetir(){
        System.out.println(ogretmen1.toString());
        System.out.println(ogretmen2.toString());
        System.out.println(ogretmen3.toString());
        System.out.println(ogretmen4.toString());
        System.out.println(ogretmen5.toString());
    }

}
