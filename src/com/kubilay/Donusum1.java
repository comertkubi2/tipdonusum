package com.kubilay;

public class Donusum1 {

    int s1=10; //global değişken, genel değişken
    int s2=20; //global değişken, genel değişken
    int s3=30;

    public void topla1(){
        int toplam; //yerel değişken, local değişken
        toplam=s1+s2+s3;
        System.out.println(toplam);
    }

    public void topla2(){
        String a1= String.valueOf(s1); //s1 sayısını metne dönüştürdüm "10"    10
        String a2= Integer.toString(s2); //"20"
        String a3= s3+"";  //"30"
        System.out.println(a1+a2+a3); //1020 30

    }

    public void topla3(){
        String x1="100";
        String x2="200";

        System.out.println(x1+x2); //100200

        int a1= Integer.parseInt(x1);
        int a2=Integer.valueOf(x2);

        System.out.println(a1+a2); //300
    }

    public void topla4(){
        byte x1=100;
        int  x2 = x1; //arka planda kendi kapalı tip dönüşümü yaptı

        int x3=50; //int 'i byte dönüştürmek için ilk önce int i
                   // Stringe dönüştürdük sonrada String i Byte dönüştürmüş olduk.
        byte x4 = Byte.parseByte(Integer.toString(x3))  ; //tip dönüşümü 1. yol
        byte x5= (byte) x3; //tip dönüşümü 2. yol

        int toplam= x1+x4;
        System.out.println(toplam);
    }

    public void topla5(){
        long x = 5;
      //  byte y = x;  hata olur açık tip dönüşümü yapmam lazım
        //zor yol
        byte z= Byte.parseByte(Long.toString(x)) ;
        //kolay yol
        byte t=  (byte) x;
    }


}
