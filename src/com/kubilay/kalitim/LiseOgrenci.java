package com.kubilay.kalitim;

public class LiseOgrenci extends Ogrenci {
    private String okulTuru;

    //constructor new komutu gelince otomatik çalışır.
    public LiseOgrenci(String ad, String soyad, String tc, String okulTuru) {
        super(ad, soyad, tc); //süper classın constructor ının çalıştır komutudur.
        this.okulTuru = okulTuru;
    }

    @Override
    public String toString() {
        return super.toString() + "-----"+ this.okulTuru ; //süper sınıfındaki toString() i çalıştır
    }

    public void aidatOde(){
        String met;
        met= super.getAd()+" "+super.getSoyad()+" "+" 1000 tl olan aidatı ödediniz....";
        System.out.println(met);
    }

    public String getOkulTuru() {
        return okulTuru;
    }

    public void setOkulTuru(String okulTuru) {
        this.okulTuru = okulTuru;
    }
}
