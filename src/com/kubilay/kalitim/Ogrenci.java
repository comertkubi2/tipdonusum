package com.kubilay.kalitim;

public class Ogrenci {
   private String ad;
   private String soyad;
   private String tc;

    public Ogrenci(String ad, String soyad, String tc) {

        this.ad = ad;
        this.soyad = soyad;
        this.tc = tc;
    }

    @Override
    public String toString() {
        return this.ad+"-----"+this.soyad+"-----"+this.tc;

    }

    //getter ve setter metotları

    public String getAd() {
        return ad;
    }

    public void setAd(String ad) {
        this.ad = ad;
    }

    public String getSoyad(){
        return this.soyad;
    }

    public void setSoyad(String soyad){
        this.soyad=soyad;
    }

    public String getTc() {
        return tc;
    }

    public void setTc(String tc) {
        this.tc = tc;
    }
}
