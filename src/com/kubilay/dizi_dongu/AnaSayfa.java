package com.kubilay.dizi_dongu;


import java.util.Random;

public class AnaSayfa {

    static Random random=new Random();  //global random tüm metotlardan erişilebilinir

    //nesneleri constructorda oluşturmak tercih edilir

    public AnaSayfa() {

    }

    public static void main(String[] args) {
       // diziOlustur();
        //diziOlusturForEach();
        //diziForEach2();
       // sayiTuret();
        sayiTuret4();
    }

    public static void diziOlustur(){

        int[] sayilar=new int[5];
        sayilar[0]=10;
        sayilar[1]=15;
        sayilar[2]=20;
        sayilar[3]=25;
        sayilar[4]=30;

        //dizinin içindeki sayıların toplamı bul
        int toplam=0;
        //dizinin içinde ilk elemandan son elemana kadar dön

        for(int i=0; i<=4 ;i++){
            toplam = toplam+sayilar[i];
        }
        System.out.println("dizinin içindeki sayılar toplamı= "+toplam);

    }

    public static void diziOlusturForEach(){
        int[] sayilar=new int[5];
        sayilar[0]=11;
        sayilar[1]=15;
        sayilar[2]=20;
        sayilar[3]=25;
        sayilar[4]=30;

        int toplam=0;

        //foreach ile dizinin elemanları üzerinde dön ve topla
        for ( int x  : sayilar ) {
            toplam=toplam+x;
        }
        System.out.println("dizinin içindeki sayılar toplamı= "+toplam);

    }

    public static void diziForEach2(){

        String[] isimler=new String[5];
        isimler[0]="ali";
        isimler[1]="veli";
        isimler[2]="ayşe";
        isimler[3]="elif";
        isimler[4]="nehir";

        String birles="";
        for( String x  : isimler ){
            birles=birles+x+"***";
        }
        System.out.println(birles);
    }

    //10 adet 0-50 arası sayı üretip ekrana yazdır
    public static void sayiTuret(){

        int rastgeleSayi;
       // Math.abs() //abs metodu static
        //10 kere çalışan bir döngünün
        for(int i=1;i<=10;i++){
            rastgeleSayi= random.nextInt(101); //0 ile 100 arasaında 20 tane rastgele sayı türetir.
            System.out.println(rastgeleSayi);
        }
    }

    public static void sayiTuret2(){
        //10 ile 15 arasında 5 tane sayı üretelim ekrana yazalım.
        int rsayi;
        for(int i=1; i<=10;i++){
            //en küçük 0 türetir ama ben en küçük 10 istiyorum,
            //en büyük 50 türetir
            //50-10=40
            rsayi= random.nextInt(6)+10 ; //en küçük 10, en büyük 50
            System.out.println(rsayi);
        }
    }

    public static void sayiTuret3(){
        // 6 elemanlı bir int dizisinin içini rastgele 50-100 arasındaki sayılarla
        //doldur ve ekrana yaz.
        int[] rsayilar=new int[6]; //index no 0-5

        for (int i=0 ;i<=5 ;i++){ //dizinin içini rastgele sayılarla doldurmuş olduk.
            rsayilar[i] = random.nextInt(51)+50;
        }

        //ekranda diziyi göster dizi olduğu içi for-each
        for ( int sayi  : rsayilar  ) {
            System.out.println(sayi);
        }
    }

    public static void sayiTuret4(){
        //5 elemanlı dizinin içini 20-50 arası sayılarla doldur ekranda göster ve toplamlarını ekrana yaz
        int[] rsayilar=new int[5];
        int toplam=0;

        for (int i=0;i<=4;i++){
            rsayilar[i]=random.nextInt(31)+20;
            toplam=toplam+rsayilar[i];
        }

        for (int sayi : rsayilar ){
            System.out.println(sayi);
        }

        System.out.println("dizinin elemanları toplaı= "+toplam);
    }

}
