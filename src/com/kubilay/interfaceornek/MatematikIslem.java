package com.kubilay.interfaceornek;
public class MatematikIslem implements IMatematikIslem {
    @Override
    public boolean isAsalSayi(int sayi) {
        for (int x=2;x<=sayi-1;x++){
            if(sayi%x==0){
                return false; //asal değil false
            }
        }
        return true;   //asal sayı true
    }
    @Override
    public boolean isTekCift(int sayi) {
        if(sayi%2==0){
            return true; //çiftler true
        }
        return false; //tek false
    }
}
