package com.kubilay.interfaceornek;

public interface IMatematikIslem {
    public boolean isAsalSayi(int sayi);
    public boolean isTekCift(int sayi);
}
