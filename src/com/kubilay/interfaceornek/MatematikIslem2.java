package com.kubilay.interfaceornek;
public class MatematikIslem2 implements IMatematikIslem {
    @Override
    public boolean isAsalSayi(int sayi) {
        int bayrak=0;
        for (int x=2;x<=sayi-1;x++){
            if(sayi%x==0){
                bayrak=1;
                break;
            }
        }
        if(bayrak==0){
            return true; //asal  true
        }else {
            return false; //asal değil  false
        }
    }

    @Override
    public boolean isTekCift(int sayi) {
        if(sayi%2==1){
            return true; //tek ise true
        }
        return false; //çift false
    }
}
