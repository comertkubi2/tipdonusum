package com.kubilay.dongu;

public class IcIceForAna {
    public static void main(String[] args) {
            icIce9();
    }

    public static void icIce1(){
       int sno=1;
        for (int i=1 ; i<=5 ; i++){  //dış döngü
            System.out.println("************************");
            for (int x=1 ; x<=3 ; x++){  // iç döngü
                System.out.println(sno +" / i="+ i +" x="+x+ " için cömert yazar");   //çalışma sayısı= dış döngü sayısı * iç döngü sayısı
                sno=sno+1;
            }

        }
    }

    public static void icIce2(){
        //      1 2 3 4 5       1 2 3 4 5        1 2 3 4 5
        for(int i=1;i<=3;i++){
            for(int x=1;x<=5;x++){
                System.out.print( x+" ");
            }
        }
    }

    public static void icIce3(){
        //      1 1 1 1 1 2 2 2 2 2 3 3 3 3 3
        for(int i=1;i<=3;i++){
            for(int x=1;x<=5;x++){
                System.out.print( i+" ");
            }
        }
    }

    public static void icIce4(){
        //      1 2 3 1 2 3 1 2 3 1 2 3 1 2 3
        for(int i=1;i<=5;i++){
            for(int x=1;x<=3;x++){
                System.out.print( x+" ");
            }
        }
    }

    public static void icIce5(){
        //      1 1 1    2 2 2    3 3 3    4 4 4    5 5 5
        for(int i=1;i<=5;i++){
            for(int x=1;x<=3;x++){
                System.out.print( i+" ");
            }
        }
    }

    public static void icIce6(){
        // 5 4 3 2 1     5 4 3 2 1    5 4 3 2 1    5 4 3 2 1
        for(int i=1;i<=4;i++){
            for(int x=5;x>=1;x--){
                System.out.print( x+" ");
            }
        }
    }

    public static void icIce7(){
        // iç içe 2 for ile  çarpım tablosunu oluşturun    1*1=1
                                                       /*  1*2=2



                                                         1*10=10
                                                         ****************
                                                         2*1=2
                                                         2*2=4
                                                         


                                                         2*10=20
                                                         *****************   */
        for(int i=1;i<=10;i++){
           // Math.random()
            //if karar yapısı
            //iç içe döngü  continue,break
            //arraylist e benzersiz i leri yazman lazım
            for(int x=1;x<=10;x++){
                System.out.println( i+" * "+x+" = "+ (i*x) );
            }
            System.out.println("***************************");
        }
    }

    public static void icIce8(){
        // 1    1 2    1 2 3    1 2 3 4    1 2 3 4 5      ekran çıktısının içiçe 2 for ile yapınız.
        //iç döngüde eleman sayısı öncekiler gibi sabit değil iç döngü ilk seferde 1, 2. de 2, 3. de 3 ....
        for (int i=1;i<=5;i++){

            for (int x=1; x<=i  ; x++){  //  x<=i yazarak iç döngüyü her seferinde farklı sayıda döndürdüm
                System.out.println(x);
            }
            System.out.println("************");
        }

    }



    public static void icIce9(){
        // 1 2 3 4 5  ***     1 2 3 4   ****  1 2 3  ***  1 2    **  1       ekran çıktısının içiçe 2 for ile yapınız.
        //iç döngüde eleman sayısı öncekiler gibi sabit değil iç döngü ilk seferde 1, 2. de 2, 3. de 3 ....
        // 5 5  5 5 5 ****** 4 4 4 4 ******* 3 3 3 **** 2 2 *** 1
        for (int i=5;i>=1;i--){

            for (int x=1; x<=i  ; x++){  //  x<=i yazarak iç döngüyü her seferinde farklı sayıda döndürdüm
                System.out.println(i);
            }
            System.out.println("************");
        }

    }



}
