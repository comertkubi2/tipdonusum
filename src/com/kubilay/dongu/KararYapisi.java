package com.kubilay.dongu;

public class KararYapisi {

    public static void main(String[] args) {
        karar3();

    }

    public static void karar1() {

        int yas = 19;
        int bul = 60;

        if (yas <= 10) {
            bul = 5;
        }

        System.out.println("bul= " + bul);

    }

    public static void karar2() {

        String yaz;
 //iki seçenekten birini şeçmek için kullanırım
        if (5 == 5) {  //true bolğu
            yaz = "MANİSA";
        } else { //false bloğu
            yaz = "İZMİR";
        }

        System.out.println(yaz);

    }

    public static void karar3() {
        int sinavNot = 65;
        if (sinavNot < 0) {
            System.out.println("SINAV NOTU 0 DAN KÜÇÜK OLAMAZ");
        } else if(sinavNot<=25) {
            System.out.println("SIFIR");
        }
        else if (sinavNot <= 44) {
            System.out.println("BİR");
        } else if (sinavNot <= 60) {
            System.out.println("İKİ");
        } else if (sinavNot <= 70) {
            System.out.println("ÜÇ");
        } else if (sinavNot <= 84) {
            System.out.println("DÖRT");
        } else if(sinavNot<=100){
            System.out.println("BEŞ");
        }else { //-1 burdan geliyor çünkü son kalan seçenek için şart yazmayız.
            System.out.println("SINAV NOTU 100 DEN BÜYÜK OLAMAZ");
        }
        System.out.println("her zaman çalışır çünkü if yapısının dışında");
    }

}
