package com.kubilay.dongu;

public class DonguAna {

    static int sayi1; //field global değişken

    public static void main(String[] args) {
        for4();
    }

    public static void for1(){
        int x;

        for(x=1 ; x<=10 ; x++ ){ // x döngü değişkeni sadece for un içinde tanımlıdır
            System.out.println( x+"."+ " cömert");
        }
        System.out.println("döngü değişkeni= " + x);
    }

    public static void for2(){
        //2 ile 40 arasındaki çift sayıları ekrana yazın
        int a;
        for (a=2 ; a <=40 ; a=a+2){
            System.out.println(a);
        }
        System.out.println("döngü değişkeni= " + a);
    }

    public static void for3(){
        // 50 den başlayıp 5 er azalan 0 kadar
        int a;
        for (a=50 ; a >= 0 ; a=a-5){
            System.out.println(a);
        }
        System.out.println("döngü değişkeninin değeri= " + a );

    }

    public static void for4(){
        // ekrana 1 4 9 16 25 36 49 64 81 100
        for (int i=1 ; i<=10 ; i++){
            int sonuc=i*i;
            System.out.println(i + " karesi= " + sonuc);
        }

    }


}
