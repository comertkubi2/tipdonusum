package com.kubilay;

public class Diziler {

    public void dizi1(){
        int[] nolar = new int[5]; // Dizi tanımladım artık kullanabilirim, nolar değişkeni içinde 5 tane tam sayı tutabilir.

        //değer girme başla
        nolar[0]=522;
        nolar[1]=1635;
        nolar[2]=1574;
        nolar[3]=1;
        nolar[4]=2;
        //değer girme bitir

        //dizinin içindeki değerleri okuma başla
        System.out.println("dizinin 3. elemanı= " + nolar[2]);
        System.out.println("dizinin 1. elemanı= " + nolar[0]);
        System.out.println("dizinin 4. elemanı= " + nolar[3]);
        System.out.println("dizinin 5. elemanı= " + nolar[4]);
        System.out.println("dizinin 2. elemanı= " + nolar[1]);
        //dizinin içindeki değerleri okuma bitir
    }

    public void dizi2(){

      //  String[] adlar=new String[4]; //dizi tanımlama 1
        String[] adlar={"cömert","hasan","ibrahim","batuhan"}; //dizi tanımlama 2 kısa yol ilk değerleri atanarak

        System.out.println("dizinin 3. elemanı= "+adlar[2]);
        System.out.println("dizinin 1. elemanı= "+adlar[0]);
        System.out.println("dizinin 4. elemanı= "+adlar[3]);
        System.out.println("dizinin 2. elemanı= "+adlar[1]);

    }

}
