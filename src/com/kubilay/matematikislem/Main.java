package com.kubilay.matematikislem;

public class Main {

   static MatematikIslemler mi=new MatematikIslemler();

    public static void main(String[] args) {
       // asalHepsiniGoster();
      //  asalOlanlarGoster();
       // asalOlmayanlarGoster();
       tekCiftHepsi();
    }

    public static void asalHepsiniGoster(){
        for(int x=2;x<=100;x++){
            String sonuc= mi.isAsalSayi(x) ? "Asal Sayıdır." : "Asal Sayı Değildir";
            System.out.println(x+ " "+sonuc);
        }
    }
    public static void asalOlanlarGoster(){
        int sayac=0;
        for(int x=2;x<=1000;x++){
            if(mi.isAsalSayi(x)){
                System.out.println(x+" Asal Sayıdır");
                sayac=sayac+1;
            }
        }

        System.out.println("2 ile 1000 arasında "+sayac + " tane asal sayı vardır");
    }
    public static void asalOlmayanlarGoster(){
        int sayac=0;
        for (int x=2;x<=10;x++){
            if(! mi.isAsalSayi(x) ){
                System.out.println(x+" Asal Değildir.");
                sayac++;
            }
        }
        System.out.println("2 ile 20 arasında asal olmayan "+sayac +" tane sayı vardır.");
    }

    public static void tekCiftHepsi(){
        for (int x=1;x<=40;x++){
            if(mi.isTekCift(x)){
                System.out.println(x+" sayısı tektir.");
            }else {
                System.out.println(x+" sayısı çifttir");
            }
        }
    }

}
