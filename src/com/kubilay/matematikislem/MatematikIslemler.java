package com.kubilay.matematikislem;

public class MatematikIslemler {

    public boolean isAsalSayi(int sayi){

        for(int x=2;x<=sayi-1;x++){
            if((sayi%x)==0){
                return false;
            }
        }
        return true; //for içindeki return false hiç çalışmazsa true döner.
    }

    public boolean isTekCift(int sayi){
        //çift sayı false, tek sayı true olsun.
        if( (sayi%2) ==0){
            return false;
        }
        return true;
    }


}
