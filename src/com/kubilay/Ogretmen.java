package com.kubilay;

public class Ogretmen {  //kalıp 1 tane olur //Ogretmen sınıfı Objectten gizli kalıtım alır.

    String ad ;       //genel değişken field //static olmayan fieldlara nesne fieldı
    String soyad ;    //field ların ilk değerleri genellikle constructor da verilir.
    String brans ;
    static String okulAd="Manisa Meslek Lisesi"; //static field lar a class field ı denir
    //constructer metot :
    // özel bir metotdur, bu classtan new ile yeni bir nesne(instance) oluştururken bu metot
    //otomatik olarak çalışır ve nesne tanımlama işlemini yapar
    public Ogretmen(String ad, String soyad, String brans) {  //field lara ilk değerlerini verdik.
        this.ad = ad;
        this.soyad = soyad;
        this.brans = brans;
    }
    public Ogretmen() {
        this.ad="cömert";
        this.soyad="kubilay";
        this.brans="bilişim";
    }

    public Ogretmen(String ad, String soyad){
        this.ad=ad;
        this.soyad=soyad;
    }

    //bu nesne metodunu ben yazdım
    public void ogretmenDenemeMetot(){ //static olmayan metotlara nesne metodu denir. ve bu kalıptan üretilen
                                        //bütün nesnelerin içerisine bu metot kopyalanır.
        System.out.println(this.ad + " bu bir deneme metodudur,");
    }

    public static void deneme2(){ // static olan metotlar a class metodu denir. bunlar her nesnenin içine kopyalanmaz
                                    //sadece 1 tane ve bu sınıf için kullanılır
        System.out.println("bu metot statictir");

    }

    @Override //Objectten gelen toString() i ez.
    public String toString() {
        String ogretmenBilgileri;
        ogretmenBilgileri=this.ad + " "+this.soyad+ " "+ this.brans ;
        return ogretmenBilgileri;
    }
}
